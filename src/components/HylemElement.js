import React from "react";
import Hylem from "./Hylem";
import {Container} from "react-bootstrap";
import '../styles/HylemElement.css';

class HylemElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hylemsList: [this.renderHylem(), <button onClick={() => {
        this.addHylemRow()
      }}>+</button>],
    };
    this.addHylemRow = this.addHylemRow.bind(this);
  }

  renderHylem() {
    return <Hylem
      onClick={() => this.handleClick()}
    />;
  }

  addHylemRow() {
    const hylemsList = this.state.hylemsList;
    this.setState({
      hylemsList: hylemsList.concat(<Hylem id={() => {
        this.state.hylemsList.map((input) => {
          return input.index;
        })
      }}/>).concat(<button onClick={() => {
        this.addHylemRow()
      }}>+</button>)
    });
  }

  render() {
    return (
      <Container class="hyleme-element">
        {this.state.hylemsList.map((input, index) => {
          return input;
        })}
      </Container>
    );
  }
}

export default HylemElement;
