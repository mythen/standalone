import React from "react";
import Form from "react-bootstrap/Form";
import {InputGroup, FormControl} from "react-bootstrap";

class ObjectElement extends React.Component {
  render() {
    return (
      <Form>
        <InputGroup className="input-group-sm">
          <InputGroup.Prepend>
            <InputGroup.Text id="inputObject">Objekt</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="text"
            placeholder="Objekt"
            aria-label="Objekt"
            aria-describedby="inputObject"
          />
          <FormControl
            type="text"
            placeholder="Objekt-Determination"
            aria-label="Objekt-Determination"
          />
        </InputGroup>
      </Form>
    )
  }
}

export default ObjectElement;
