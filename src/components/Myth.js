import React from "react";
import {Form, FormControl, InputGroup} from "react-bootstrap";
import '../styles/Myth.css';

class Myth extends React.Component {
  render() {
    return (
      <div className="myth-selection">
        <Form>
          <InputGroup className="input-group-sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputMyth">Mythos</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              type="text"
              placeholder="Mythos"
              aria-label="Mythos"
              aria-describedby="inputMyth"
            />
          </InputGroup>
        </Form>
      </div>
    );
  }
}

export default Myth;
