import React from "react";
import Form from "react-bootstrap/Form";
import {InputGroup, FormControl} from "react-bootstrap";

class Predicate extends React.Component {
  render() {
    return (
      <Form>
        <InputGroup className="input-group-sm">
          <InputGroup.Prepend>
            <InputGroup.Text id="inputPredicate">Prädikat</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="text"
            placeholder="Prädikat"
            aria-label="Prädikat"
            aria-describedby="inputPredicate"
          />
          <FormControl
            type="text"
            placeholder="Prädikat-Determination"
            aria-label="Prädikat-Determination"
          />
        </InputGroup>
      </Form>
    )
  }
}

export default Predicate;
