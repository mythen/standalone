import React from "react";
import Subject from "./Subject";
import Predicate from "./Predicate";
import ObjectElement from "./ObjectElement";
import {FormCheck, FormControl, InputGroup} from "react-bootstrap";
import Row from "react-bootstrap/Row";

class Hylem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: true,
      subjectList: [this.renderSubject(), <button onClick={() => {
        this.addSubjectRow()
      }}>+</button>],
      objectList: [this.renderObjectElement(), <button onClick={() => {
        this.addObjectRow()
      }}>+</button>],
    };
    this.addSubjectRow = this.addSubjectRow.bind(this);
    this.addObjectRow = this.addObjectRow.bind(this);
  }

  renderSubject() {
    return <Subject
      onClick={() => this.handleClick()}
    />;
  }

  renderPredicate() {
    return <Predicate
      onClick={() => this.handleClick()}
    />;
  }

  renderObjectElement() {
    return <ObjectElement
      onClick={() => this.handleClick()}
    />;
  }

  addSubjectRow() {
    const subjectList = this.state.subjectList;
    this.setState({
      subjectList: subjectList.concat(<Subject id={() => {
        this.state.subjectList.map((input) => {
          return input.index;
        })
      }}/>).concat(<button onClick={() => {
        this.addSubjectRow()
      }}>+</button>)
    });
  }

  addObjectRow() {
    const objectList = this.state.objectList;
    this.setState({
      objectList: objectList.concat(<ObjectElement id={() => {
        this.state.objectList.map((input) => {
          return input.index;
        })
      }}/>).concat(<button onClick={() => {
        this.addObjectRow()
      }}>+</button>)
    });
  }

  /**
   * Switch from explicit to implicit and back
   * @returns {boolean}
   */
  changeFieldType() {
    this.setState({
      isChecked: !this.state.isChecked,
    });
  }

  render() {
    return (
      <div>
        <Row>
          {this.state.subjectList.map((input, index) => {
            return input;
          })}
        </Row>
        <Row>
          {this.renderPredicate()}
        </Row>
        <Row>
          {this.state.objectList.map((input, index) => {
            return input;
          })}
        </Row>
        <Row>
          <InputGroup className="input-group-sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputLine">Zeile</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              type="text"
              placeholder=""
              aria-label="Zeile"
              aria-describedby="inputLine"
            />
          </InputGroup>
        </Row>
        <Row>
          <InputGroup className="input-group-sm mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputExpImp">Hylemtyp</InputGroup.Text>
            </InputGroup.Prepend>
            {['explizit', 'implizit'].map((type) => (
              <FormCheck
                inline
                type="radio"
                label={type}
                value={type}
                name="explimpl"
                id={`explimpl-${type}`}
                checked={this.state.isChecked}
                onChange={this.changeFieldType}
              />
            ))}
          </InputGroup>
        </Row>
      </div>
    );
  }
}

export default Hylem;
