import React from "react";
import HylemElement from "./HylemElement";
import {Button, Tab, Tabs} from "react-bootstrap";
import Myth from "./Myth";
import '../styles/Sequence.css';

class Sequence extends React.Component {
  render() {
    return (
      <div className="app">
        <h1>Hylemsequenzen</h1>
        <div className="sequences">
          <div className="sequence-wrapper">
            <Myth/>

            <Tabs id="sequence-tabs">
              <Tab title="Stoff" eventKey="stoff">
                <HylemElement/>
              </Tab>
              <Tab title="Chronologisch" eventKey="chronologisch">
                <HylemElement/>
              </Tab>
            </Tabs>
            <Button>Save</Button>
          </div>
        </div>
      </div>
    );
  }
}

export default Sequence;
