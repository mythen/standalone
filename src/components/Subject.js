import React from "react";
import Form from "react-bootstrap/Form";
import {InputGroup, FormControl} from "react-bootstrap";

class Subject extends React.Component {
  render() {
    return (
      <Form>
        <InputGroup className="input-group-sm">
          <InputGroup.Prepend>
            <InputGroup.Text id="inputSubject">Subjekt</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="text"
            placeholder="Subjekt"
            aria-label="Subjekt"
            aria-describedby="inputSubject"
          />
          <FormControl
            type="text"
            placeholder="Subjekt-Determination"
            aria-label="Subjekt-Determination"
          />
        </InputGroup>
      </Form>
    )
  }
}

export default Subject;
